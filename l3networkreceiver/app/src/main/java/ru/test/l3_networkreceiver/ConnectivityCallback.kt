package ru.test.l3_networkreceiver

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.widget.TextView

class ConnectivityCallback(private val textView: TextView) : ConnectivityManager.NetworkCallback() {
    override fun onCapabilitiesChanged(network: Network, capabilities: NetworkCapabilities) {
        val connected = capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        if (connected) {
            textView.post {
                textView.text = textView.context.getText(R.string.network_on)
                textView.background.setTint(textView.context.getColor(R.color.yes_connection))
            }
        }
    }
    override fun onLost(network: Network) {
        textView.post {
            textView.text = textView.context.getText(R.string.network_off)
            textView.background.setTint(textView.context.getColor(R.color.no_connection))
        }
    }
}