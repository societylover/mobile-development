package ru.test.l7_remotedatabase.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.test.l7_remotedatabase.databinding.TodoListFragmentBinding
import ru.test.l7_remotedatabase.model.Todo
import ru.test.l7_remotedatabase.viewmodel.TodoListViewModel

@AndroidEntryPoint
class TodoListFragment : Fragment() {

    companion object {
        fun newInstance() = TodoListFragment()
    }

    private var _binding: TodoListFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TodoListViewModel by viewModels()

    private val itemClicksCallback =  object : ItemButtonsCallback {
        override fun updateTodo(todo: Todo) {
            viewModel.update(todo)
        }

        override fun updateTodoDate(todo: Todo) {
            val datePicker = DatePickerFragment(todo) { updateTodo(todo) }
            datePicker.show(parentFragmentManager, "datePicker")
        }

        override fun removeTodo(todo: Todo) {
            viewModel.delete(todo)
        }
    }

    private val todoAdapter = TodoListRecyclerView(itemClicksCallback)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        TodoListFragmentBinding.inflate(inflater, container, false).also { _binding = it }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.todosView.adapter = todoAdapter
        binding.todosView.layoutManager = LinearLayoutManager(context)

        viewModel.allTodos.observe(viewLifecycleOwner, { todoAdapter.setItems(it) })

        binding.add.setOnClickListener { viewModel.insert(Todo()) }
    }
}

