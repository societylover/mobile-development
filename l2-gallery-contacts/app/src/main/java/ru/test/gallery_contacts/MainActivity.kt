package ru.test.gallery_contacts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.Manifest.permission.READ_CONTACTS
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.content.Intent
import android.provider.ContactsContract
import android.provider.MediaStore
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts

class MainActivity : AppCompatActivity() {

    private val requestCode = 100
    private lateinit var adapter: SimpleAdapter
    private lateinit var itemsList: ArrayList<Map<String,String>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getPermissions()
        itemsList = ArrayList()
        adapter = SimpleAdapter (this, itemsList, android.R.layout.simple_list_item_2, arrayOf("name", "phone"),
            intArrayOf(android.R.id.text1, android.R.id.text2))

        contact_list.adapter = adapter
        show_image_btn.setOnClickListener{ getContent.launch("image/*") }
    }

    private val getContent = registerForActivityResult(ActivityResultContracts.GetContent()) {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra("URI", it)
        startActivity(intent)
    }

    private fun getPermissions() =
        requestPermissions(arrayOf(READ_CONTACTS, READ_EXTERNAL_STORAGE), requestCode)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            if(requestCode == this.requestCode) {
                if (permissions.contains(READ_CONTACTS) && permissions.contains(
                        READ_EXTERNAL_STORAGE)) {
                    setContactsData()
                } else {
                    Toast.makeText(this, "NO PERMISSIONS GRANTED", Toast.LENGTH_LONG).show()
                }
            }
    }

    @SuppressLint("Recycle")
    private fun setContactsData() {
        val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)

        if (cursor?.count!! > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phoneNumber = (cursor.getString(
                    cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()

                if (phoneNumber > 0) {
                    val cursorPhone = contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", arrayOf(id), null)
                    if(cursorPhone?.count!! > 0 && cursorPhone.moveToNext()) {
                        itemsList.add(hashMapOf(Pair("name", name), Pair("phone", cursorPhone.getString(
                                 cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)))))
                    }
                    cursorPhone.close()
                }
            }
        } else Toast.makeText(this, "NO CONTACTS FOUND", Toast.LENGTH_LONG).show()
        adapter.notifyDataSetChanged()
        cursor.close()
    }
}