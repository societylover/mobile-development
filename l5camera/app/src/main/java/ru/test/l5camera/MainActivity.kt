package ru.test.l5camera

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.mlkit.vision.barcode.BarcodeScanning
import ru.test.l5camera.databinding.ActivityMainBinding
import java.lang.IllegalStateException
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var cameraProvider: ProcessCameraProvider? = null
    private var cameraSelector: CameraSelector? = null
    private var previewUseCase: Preview? = null
    private var lensFacing = CameraSelector.LENS_FACING_BACK
    private var analysisUseCase: ImageAnalysis? = null
    private lateinit var imageProcessing: ImageProcessing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        imageProcessing = ImageProcessing(binding.tvScannedData)
        setContentView(binding.root)

        cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()
        ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(application)).get(CameraXViewModel::class.java)
            .processCameraProvider
            .observe(this) { provider: ProcessCameraProvider? ->
                    cameraProvider = provider
                    // 1. Проверяем разрешение на камеру
                    if (isCameraPermissionGranted()) {
                        // Запускаем камеру
                        bindPreviewUseCase()
                        bindAnalyseUseCase()
                    } else {
                        // Запрашиваем разрешения
                        requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CODE)
                    }
            }
    }

    private fun bindPreviewUseCase() {
        if (previewUseCase != null) cameraProvider!!.unbind(previewUseCase)

        previewUseCase = Preview.Builder().setTargetRotation(binding.previewView.display.rotation).build()
        // Закрепляем binding.previewView за сигналом камеры по обработке данных
        previewUseCase!!.setSurfaceProvider(binding.previewView.surfaceProvider)
        try {
            // Привяжем работу камеры к жизненному циклу приложения
            cameraProvider!!.bindToLifecycle(this, cameraSelector!!, previewUseCase)
        } catch (e: IllegalStateException) {
            Log.e(TAG, e.message ?: "IllegalStateException")
        } catch (illegalArgumentException: IllegalArgumentException) {
            Log.e(TAG, illegalArgumentException.message ?: "IllegalArgumentException")
        }
    }

    private fun bindAnalyseUseCase() {
        // Установим тип кодов, которые обрабатываются
        val barcodeScanner = BarcodeScanning.getClient(imageProcessing.options)
        analysisUseCase = ImageAnalysis.Builder().setTargetRotation(binding.previewView.display.rotation).build()

        // Отбработка данных в отдельном потоке в обработчике processImageProxy
        analysisUseCase?.setAnalyzer(Executors.newSingleThreadExecutor(), { imageProcessing.processImageProxy(barcodeScanner, it) })

        try {
            cameraProvider!!.bindToLifecycle(this, cameraSelector!!, analysisUseCase)
        } catch (illegalStateException: IllegalStateException) {
            Log.e(TAG, illegalStateException.message ?: "IllegalStateException")
        } catch (illegalArgumentException: IllegalArgumentException) {
            Log.e(TAG, illegalArgumentException.message ?: "IllegalArgumentException")
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE) {
            if (isCameraPermissionGranted()) {
                // Запускаем камеру
            } else {
                println("No camera permission")
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun isCameraPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            baseContext,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    companion object {
        private const val REQUEST_CODE = 1
        private const val TAG = "MainActivity"
    }
}