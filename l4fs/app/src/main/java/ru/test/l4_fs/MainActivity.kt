package ru.test.l4_fs

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.MutableLiveData
import ru.test.l4_fs.databinding.ActivityMainBinding
import java.io.File

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val path = MutableLiveData<File>()
    private val adapter: ArrayAdapter<String> by lazy { ArrayAdapter<String>(this, android.R.layout.simple_list_item_1) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.listview.adapter = adapter
        path.observe(this, { updateViewList(it) })

        path.value = dataDir

        binding.addFile.setOnClickListener {
            startActivity(Intent(this, MainActivity2::class.java))
            // createFile()
        }
        binding.addFolder.setOnClickListener {
            startActivity(Intent(this, MainActivity3::class.java))

            // createFolder()
        }

        binding.listview.onItemLongClickListener = onLongClickListener
        binding.listview.onItemClickListener = clickListener

        binding.back.setOnClickListener {
            println(path.value.toString())
            val i = path.value.toString().indexOfLast { it == '/' }
            val file = File(path.value.toString().substring(0..i))
            if (path.value!! != dataDir) path.value = file
        }
    }


    private val clickListener = AdapterView.OnItemClickListener { p0, p1, p2, p3 ->
        val item = getFileTypeAndName(p2) ?: return@OnItemClickListener
        if (item.first) {
            path.value = File("${path.value.toString()}/${item.second}")
        } else {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.root, FileFragment.newInstance(File("${path.value.toString()}/${item.second}")), "file check")
                //addToBackStack("file")
                commitAllowingStateLoss()
                }
            updateButtonState()
            }
        }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            updateButtonState()
            path.value = path.value
            super.onBackPressed()
        } else {
            binding.back.performClick()
        }
    }

    private fun updateButtonState() {
        if (!binding.addFile.isVisible) {
            binding.addFile.visibility = View.VISIBLE
            binding.addFolder.visibility = View.VISIBLE
            binding.back.visibility = View.VISIBLE
        } else {
            binding.addFile.visibility = View.INVISIBLE
            binding.addFolder.visibility = View.INVISIBLE
            binding.back.visibility = View.INVISIBLE
        }
    }

    private val onLongClickListener = object : AdapterView.OnItemLongClickListener {
        override fun onItemLongClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long): Boolean {
            val item = getFileTypeAndName(p2) ?: return false
            if (item.first) {
                File(path.value!!, item.second).deleteRecursively()
            } else {
                File(path.value!!, item.second).delete()
            }
            updateViewList(path.value!!)
            return true
        }
    }

    private fun getFileTypeAndName(pos: Int) : Pair<Boolean, String>? {
        val item = adapter.getItem(pos) ?: return null
        val itemName = item.removeRange(0..6).replace(" ", "")
        return Pair(item.startsWith(directoryPrefix), itemName)
    }

    private fun createFolder() {
        val folder = File(path.value!!, binding.name.text.toString())
        if (!folder.exists()) {
            folder.mkdirs()
            path.value = path.value!!
        } else makeToast("Такая папка уже есть!")
    }

    private fun createFile() {
        val file = File("${path.value!!}/${binding.name.text.toString()}")
        if (file.createNewFile()) {
            path.value = path.value!!
        } else makeToast("Такой файл уже есть!")
    }

    fun makeToast(text: String) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()

    private fun updateViewList(file: File) {
        adapter.clear()
        if (file.listFiles() != null) {
            for (item in file.listFiles()!!) {
                adapter.add("${if (item.isDirectory) directoryPrefix else filePrefix} ${item.name}")
            }
        }
    }

    companion object {
        const val directoryPrefix = "[DIR  ]"
        const val filePrefix = "[FILE ]"
    }
}