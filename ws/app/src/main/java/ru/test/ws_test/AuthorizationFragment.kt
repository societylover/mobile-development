package ru.test.ws_test

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.AccountPicker
import ru.test.ws_test.databinding.AuthorizationFragmentBinding

class AuthorizationFragment : Fragment() {

    private var googleSignInClient: GoogleSignInClient? = null

    lateinit var binding: AuthorizationFragmentBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestId()
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.authorization_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = AuthorizationFragmentBinding.inflate(layoutInflater)

//        binding.authorizeButton.setOnClickListener {
//            val intent = googleSignInClient!!.signInIntent
//            registerForActivityResult()
//            startActivityForResult(intent, RC_SIGN_IN)
//        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            AuthorizationFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}