package ru.test.l6_localbase.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Todo(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    var description: String = "",
    var datetime: String? = DateParameters.getCurrentDateParametersAsString()
)
