package ru.test.l10notififactions.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.test.l10notififactions.databinding.MessageItemBinding
import ru.test.l10notififactions.extensions.toFormattedString
import ru.test.l10notififactions.model.ItemClick
import ru.test.l10notififactions.model.Message

class Adapter(private val itemClick: ItemClick) : RecyclerView.Adapter<Adapter.MessageViewHolder>() {

    private val messages: MutableList<Message> = mutableListOf()

    fun addItem(newMessage: Message) {
        messages.add(newMessage)
        notifyItemInserted(messages.size-1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MessageViewHolder(MessageItemBinding.inflate(LayoutInflater.from(parent.context),parent, false))

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) = holder.bind(messages[position])

    override fun getItemCount() = messages.size

    inner class MessageViewHolder(private val binding: MessageItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(message: Message) {
            binding.apply {
                time.text = message.sentTime.toFormattedString()
                from.text = message.from
                messagedata.text = message.data
            }
            binding.messagedata.setOnClickListener{
                println(1)
                itemClick.messageClicked(message) }
        }
    }
}