package ru.test.l9sensors

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var textView: TextView

    private val lightListener = object : Light.Companion.LightListener {
        override fun onLightChanged(lightLevel: Float) {
            if (lightLevel > 0.1) {
                textView.text = "Светлее"
                player.playLight()
            }
            else {
                textView.text = "Темнее"
                player.playDark()
            }
        }
    }

    private val acceleratorListener = object : LinearAccelerator.Companion.AcceleratorListener {
        override fun onTranslation(tx: Float, ty: Float, tz: Float) {
            when {
                ty >  1.0f -> {
                    player.playDown()
                    textView.text = "Вниз"
                }
                ty < -1.0f -> {
                    player.playUp()
                    textView.text = "Вверх"
                }
                tx >  1.0f -> {
                    player.playForward()
                    textView.text = "Вперед"
                }
                tx < -1.0f -> {
                    player.playBack()
                    textView.text = "Назад"
                }
            }
        }
    }

    private lateinit var lightHandler: Light
    private lateinit var acceleratorHandler: LinearAccelerator
    private lateinit var player: SoundPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById(R.id.stateView)

        lightHandler = Light(this, lightListener)
        acceleratorHandler = LinearAccelerator(this, acceleratorListener)
        player = SoundPlayer(this)

        lightHandler.register()
        acceleratorHandler.register()
    }

    override fun onDestroy() {
        lightHandler.unregister()
        acceleratorHandler.unregister()

        super.onDestroy()
    }
}