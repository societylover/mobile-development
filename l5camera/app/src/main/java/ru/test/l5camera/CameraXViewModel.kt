package ru.test.l5camera

import android.app.Application
import android.util.Log
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.util.concurrent.ExecutionException

class CameraXViewModel(app: Application) : AndroidViewModel(app) {
    private var cameraProviderLiveData: MutableLiveData<ProcessCameraProvider>? = null

    val processCameraProvider: LiveData<ProcessCameraProvider>
        get(){
            if (cameraProviderLiveData == null) {
                cameraProviderLiveData = MutableLiveData()

                val cameraProviderFuture = ProcessCameraProvider.getInstance(getApplication())
                cameraProviderFuture.addListener({
                    try {
                        // Получаем данные с камеры и сохраним в LiveData cameraProviderLiveData
                        cameraProviderLiveData!!.value = cameraProviderFuture.get()
                    } catch (e: ExecutionException) {
                        Log.e(TAG, "Unhandled exception", e)
                    }}, ContextCompat.getMainExecutor(getApplication()))
            }
            return cameraProviderLiveData!!
        }

    companion object {
        private const val TAG = "CameraXViewModel"
    }
}