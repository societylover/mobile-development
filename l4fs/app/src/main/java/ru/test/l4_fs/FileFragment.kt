package ru.test.l4_fs

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.test.l4_fs.databinding.FileFragmentBinding
import java.io.*
import android.content.Context.MODE_PRIVATE
import android.widget.Toast


class FileFragment(var item: File) : Fragment() {

    companion object {
        fun newInstance(item: File) = FileFragment(item)
    }

    private var _binding: FileFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel = FileViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        FileFragmentBinding.inflate(inflater, container, false).also { _binding = it }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println(item.name)

        binding.fileName.setText(item.name)
        readText()
        binding.saveUpdates.setOnClickListener { updateData() }
    }

    private fun readText() {
        var data = ""
        try {
            val fis = FileInputStream(item)
            val inp = DataInputStream(fis)
            val br = BufferedReader(InputStreamReader(inp))
            var strLine: String?
            while (br.readLine().also { strLine = it } != null) {
                data += strLine
            }
            inp.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        binding.fileValue.setText(data)
    }


    private fun updateData() {
        item.writeText(binding.fileValue.text.toString())
        val newName = binding.fileName.text.toString()
        if (newName != item.name) {
            val newPath = item.path.toString().replace(item.name, "")
            val newFile = File("$newPath/$newName")
            if (newFile.createNewFile()) {
                item.renameTo(newFile)
            } else {
                Toast.makeText(requireContext(), "Файл уже присутствует! Имя не обновлено", Toast.LENGTH_SHORT).show()
            }
        }

        activity?.onBackPressed()
    }
}