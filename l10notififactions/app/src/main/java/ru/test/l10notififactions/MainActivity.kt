package ru.test.l10notififactions

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import ru.test.l10notififactions.databinding.ActivityMainBinding
import ru.test.l10notififactions.extensions.toFormattedString
import ru.test.l10notififactions.model.ItemClick
import ru.test.l10notififactions.model.Message
import ru.test.l10notififactions.model.MessagesInstance.messages
import ru.test.l10notififactions.views.Adapter

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    private lateinit var notificationManager : NotificationManager

    private val clickHandler: ItemClick = object : ItemClick {
        override fun messageClicked(message: Message) {
             val builder = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                 .setSmallIcon(R.drawable.ic_launcher_background)
                 .setContentTitle("Ты нажал на сообщение")
                 .setContentText("${message.sentTime.toFormattedString().replace("\n", "")} ${message.from}: ${message.data}")
                 .setAutoCancel(true)
                 .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            notificationManager.notify(NOTIFICATION_ID, builder.build())
        }
    }

    private val adapter = Adapter(clickHandler)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.messages.adapter = adapter
        messages.observe(this, { adapter.addItem(it) })
        setContentView(binding.root)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    companion object {
        const val NOTIFICATION_ID = 101
        const val CHANNEL_ID = "channelID"
    }
}

