package ru.test.l6_localbase

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Initializer : Application() {
}
