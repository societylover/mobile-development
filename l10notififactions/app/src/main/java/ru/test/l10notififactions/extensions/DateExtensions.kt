package ru.test.l10notififactions.extensions

import java.text.SimpleDateFormat
import java.util.*

fun Long.toFormattedString() : String {
    val format = SimpleDateFormat("dd/MM/\nyyyy", Locale.US)
    return format.format(Date(this))
}