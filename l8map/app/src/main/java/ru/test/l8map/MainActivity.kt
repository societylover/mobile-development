package ru.test.l8map

import android.Manifest.permission.*
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*

class MainActivity : AppCompatActivity() {
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val astuPosition = LatLng(46.373909, 48.054068) // ASTU, 9

    private var locationRequest: LocationRequest = LocationRequest.create()

    private lateinit var astu9Marker: Marker
    private lateinit var myMarker: Marker

    private var results: FloatArray  = floatArrayOf(0f)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mapFragment = SupportMapFragment.newInstance()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, mapFragment)
            .commit()

        mapFragment.getMapAsync(onMapReadyCallback)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (20).toLong()

        startLocationUpdates()
    }

    private var locationCallback = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult) {
                Location.distanceBetween(p0.locations.last().latitude, p0.locations.last().longitude, astuPosition.latitude, astuPosition.longitude, results)
                myMarker.position = (LatLng(p0.locations.last().latitude, p0.locations.last().longitude)) 
                if (results[0] < 15) {
                    astu9Marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                    Toast.makeText(this@MainActivity, "Ты около 9 корпуса!", Toast.LENGTH_SHORT).show()
                }
                else {
                    astu9Marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                }
        }
    }

    private val onMapReadyCallback = OnMapReadyCallback {
        astu9Marker = it.addMarker(MarkerOptions().position(astuPosition).title("ASTU 9").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))!!
        myMarker = it.addMarker(MarkerOptions().position(LatLng(46.373545, 48.053819)).title("YOU").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))!!
        it.moveCamera(CameraUpdateFactory.newLatLngZoom(astuPosition, 17f))
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, ACCESS_NETWORK_STATE), 100)
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
    }
}