package ru.test.l9sensors

import android.content.Context
import android.hardware.SensorEventListener
import android.hardware.SensorManager

abstract class SensorBasic(context: Context, sensorType: Int) {
    private val sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val sensor = sensorManager.getDefaultSensor(sensorType)

    lateinit var sensorEventListener: SensorEventListener

    fun register() {
        sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    fun unregister() {
        sensorManager.unregisterListener(sensorEventListener)
    }
}