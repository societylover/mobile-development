package ru.test.l7_remotedatabase.model

import java.util.*

object DateParameters {
    /**
     * @return day month year
     */
    fun getCurrentDateParameters() : Triple<Int, Int, Int> {
        val c = Calendar.getInstance()
        return Triple(c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.MONTH) + 1, c.get(Calendar.YEAR))
    }

    /**
     * @return day month year as string
     */
    fun getCurrentDateParametersAsString() : String {
        val date = getCurrentDateParameters()
        return "${date.first}\n${date.second}\n${date.third}"
    }
}