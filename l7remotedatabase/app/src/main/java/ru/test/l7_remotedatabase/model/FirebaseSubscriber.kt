package ru.test.l7_remotedatabase.model

interface FirebaseSubscriber {
    fun updateToRemoteDataChanged (data: List<Todo>)
}