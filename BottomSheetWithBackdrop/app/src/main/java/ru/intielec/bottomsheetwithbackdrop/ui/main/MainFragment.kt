package ru.intielec.bottomsheetwithbackdrop.ui.main

import android.Manifest.permission
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ru.intielec.bottomsheetwithbackdrop.R

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity?.findViewById<AppCompatTextView>(R.id.num) != null) println("НАШЕЛ")
        activity?.findViewById<AppCompatTextView>(R.id.num)?.setOnClickListener {
            println("ты нажал!")
            val intent = Intent(
                Intent.ACTION_CALL,
                Uri.parse("tel: +79880647177")
            )
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    permission.CALL_PHONE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                startActivity(intent)
            } else {
                requestPermissions(arrayOf(permission.CALL_PHONE), 1)
            }
        }
    }

}