package ru.test.l9sensors

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener

class LinearAccelerator(context: Context, listener: AcceleratorListener) : SensorBasic(context, Sensor.TYPE_LINEAR_ACCELERATION) {

    init {
        sensorEventListener = object : SensorEventListener {
            override fun onSensorChanged(p0: SensorEvent) {
                listener.onTranslation(p0.values[0], p0.values[1], p0.values[2]);
            }

            override fun onAccuracyChanged(p0: Sensor?, p1: Int) { }
        }
    }

    companion object {
        interface AcceleratorListener {
            fun onTranslation(tx: Float, ty: Float, tz: Float)
        }
    }
}

