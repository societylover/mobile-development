package ru.test.l6_localbase.model

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TodoRepository @Inject constructor(private val databaseDao: TodoDao) {

    val allTodos: Flow<List<Todo>> = databaseDao.allTodos()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(todo: Todo) {
        databaseDao.insert(todo)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun delete(todo: Todo) {
        databaseDao.delete(todo)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteAll() {
        databaseDao.deleteAll()
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getSize() = databaseDao.getTableSize()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(todo: Todo) {
        databaseDao.update(todo)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getTodoByID(id: Int) : Todo {
        return databaseDao.getTodoByID(id)
    }
}