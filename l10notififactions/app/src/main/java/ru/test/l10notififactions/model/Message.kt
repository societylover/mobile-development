package ru.test.l10notififactions.model

data class Message(
    val from: String?,
    val data: String,
    val sentTime: Long
)
