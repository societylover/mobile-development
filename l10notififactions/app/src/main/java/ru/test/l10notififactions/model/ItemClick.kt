package ru.test.l10notififactions.model

interface ItemClick {
    fun messageClicked(message: Message)
}