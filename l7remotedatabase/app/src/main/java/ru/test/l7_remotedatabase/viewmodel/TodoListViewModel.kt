package ru.test.l7_remotedatabase.viewmodel

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.test.l7_remotedatabase.model.FirebaseSubscriber
import ru.test.l7_remotedatabase.model.Todo
import ru.test.l7_remotedatabase.model.TodoRepository
import javax.inject.Inject

@HiltViewModel
class TodoListViewModel @Inject constructor(private val repository: TodoRepository) : ViewModel(),
    FirebaseSubscriber {
    var allTodos: MutableLiveData<List<Todo>> = MutableLiveData()

    init {
        repository.subscribe(this)
    }

    fun insert(todo: Todo) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(todo)
        allTodos.postValue(repository.allTodos)
    }

    fun delete(todo: Todo) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(todo)
    }

    fun update(todo: Todo) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(todo)
    }

    override fun updateToRemoteDataChanged(data: List<Todo>) {
        allTodos.value = data
    }

    override fun onCleared() {
        super.onCleared()
        repository.unsubscribe()
    }
}