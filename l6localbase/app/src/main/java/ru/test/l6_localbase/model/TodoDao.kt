package ru.test.l6_localbase.model

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface TodoDao {
    @Query("SELECT * FROM Todo ORDER BY uid")
    fun allTodos() : Flow<List<Todo>>

    @Query("SELECT * FROM Todo WHERE uid = :id")
    fun getTodoByID(id: Int) : Todo

    @Query("SELECT COUNT(*) FROM Todo")
    fun getTableSize() : Int

    @Insert
    fun insert(todo: Todo)

    @Delete
    fun delete(todo: Todo)

    @Query("DELETE FROM Todo")
    fun deleteAll()

    @Update
    fun update(todo: Todo)
}