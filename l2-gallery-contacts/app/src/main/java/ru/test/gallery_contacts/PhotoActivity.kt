package ru.test.gallery_contacts

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_photo2.*

class PhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo2)
        val imageURI = intent.extras?.getParcelable<Uri>("URI")
        image_view.setImageURI(imageURI)
    }
}