package ru.test.ws_test

import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString

class WebSocketImpl(private val textView: AppCompatTextView) : WebSocketListener() {

    override fun onOpen(webSocket: WebSocket, response: Response) {
        textView.post {
            Toast.makeText(textView.context, "WebSocket onOpen", Toast.LENGTH_LONG).show()
        }
    }

    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
        textView.post {
            Toast.makeText(textView.context, "WebSocket onClosed", Toast.LENGTH_LONG).show()
        }
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        textView.post {
            textView.text = text
        }
    }

    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
        textView.post {
            textView.text = bytes.hex()
        }
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        textView.post {
            t.printStackTrace()
            Toast.makeText(textView.context, "WebSocket onFailure", Toast.LENGTH_LONG).show()
        }
    }
}