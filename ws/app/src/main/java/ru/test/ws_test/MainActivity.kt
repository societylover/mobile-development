package ru.test.ws_test

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import ru.test.ws_test.databinding.ActivityMainBinding
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var socket: WebSocketImpl // by lazy { WebSocketImpl(binding.textView) }

    private val request = Request.Builder().url("wss://192.168.1.108:8080").build()
    private lateinit var client: OkHttpClient
    private lateinit var ws: WebSocket // = client.newWebSocket(request, socket)

    private fun debugAcceptAllSockets() {
        val trustAllCerts = arrayOf<TrustManager>(
            @SuppressLint("CustomX509TrustManager") object : X509TrustManager {
                @SuppressLint("TrustAllX509TrustManager")
                override fun checkClientTrusted(p0: Array<out X509Certificate>?,p1: String?) { }
                @SuppressLint("TrustAllX509TrustManager")
                override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) { }
                override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
        })

        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())

        val sslSocketFactory = sslContext.socketFactory

        client = OkHttpClient.Builder()
            .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            .hostnameVerifier{ _, _ -> true }.build()

    }

    private fun releaseClientInit() {
        client = OkHttpClient()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        supportFragmentManager.beginTransaction()
//            .replace(R.id.container,AuthorizationFragment(), "")
//            .addToBackStack("test")
//            .commit()

        binding.text.setText("{\"event\": \"get\", \"user\" : {\"id\" : 1}}")


        if (BuildConfig.BUILD_TYPE == appDebugType)  {
            Log.e("debugType", "true")
            debugAcceptAllSockets()
        }
        else {
            Log.e("debugType", "false")
            releaseClientInit()
        }

        socket = WebSocketImpl(binding.textView)
        ws = client.newWebSocket(request, socket)

        binding.send.setOnClickListener {
            ws.send(binding.text.text.toString())
        }

        binding.send.postDelayed({
                                 ws.send("delayed action")
        }, 5000L)
    }

    // SingIn check

    override fun onStart() {
        super.onStart()
//        val account = GoogleSignIn.getLastSignedInAccount(this)
//
//        if (account != null) {
//            val intent = Intent(this@SignInActivity, UserProfile::class.java)
//            startActivity(intent)
//        }
    }


    companion object {
        const val appDebugType = "debug"
    }
}