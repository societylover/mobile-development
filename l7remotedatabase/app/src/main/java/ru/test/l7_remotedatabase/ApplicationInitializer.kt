package ru.test.l7_remotedatabase

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ApplicationInitializer : Application() {
}
