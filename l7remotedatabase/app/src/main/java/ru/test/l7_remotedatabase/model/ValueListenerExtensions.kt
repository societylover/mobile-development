package ru.test.l7_remotedatabase.model

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

fun create(onDataChanged: (DataSnapshot) -> Unit) : ValueEventListener {
    return object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) = onDataChanged(snapshot)
        override fun onCancelled(error: DatabaseError) = println("Firebase error: ${error.message}")
    }
}