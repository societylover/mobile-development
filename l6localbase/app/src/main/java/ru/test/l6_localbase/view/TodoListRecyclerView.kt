package ru.test.l6_localbase.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.test.l6_localbase.R
import ru.test.l6_localbase.model.Todo

class TodoListRecyclerView(private val buttonsCallback: ItemButtonsCallback) : RecyclerView.Adapter<TodoListRecyclerView.TodoViewHolder>(){

    private var todos: MutableList<Todo> = mutableListOf()

    fun setItems(todos: List<Todo>) {
        this.todos.clear()
        this.todos.addAll(todos)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TodoViewHolder(LayoutInflater.from(parent.context).inflate(
        R.layout.recycler_item, parent, false))

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) = holder.bind(todos[position])

    override fun getItemCount() = todos.size

    inner class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bind(todo: Todo) {
            itemView.apply {
                val datetime: TextView = findViewById(R.id.datetime)
                val description: TextView = findViewById(R.id.description)
                val datetimePicker: ImageButton = findViewById(R.id.calendar_button)
                val update: ImageButton = findViewById(R.id.update)
                val remove: ImageButton = findViewById(R.id.remove)

                update.setOnClickListener {
                    todo.description = description.text.toString()
                    buttonsCallback.updateTodo(todo) }
                description.setOnEditorActionListener { v, _, _ ->
                    todo.description = v.text.toString()
                    buttonsCallback.updateTodo(todo)
                    true
                }

                datetimePicker.setOnClickListener { buttonsCallback.updateTodoDate(todo) }
                remove.setOnClickListener { buttonsCallback.removeTodo(todo) }

                datetime.text = todo.datetime
                description.text = todo.description
            }
        }
    }
}