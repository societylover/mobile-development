package ru.test.l7_remotedatabase.view

import ru.test.l7_remotedatabase.model.Todo

interface ItemButtonsCallback {
    fun updateTodo(todo: Todo)
    fun updateTodoDate(todo: Todo)
    fun removeTodo(todo: Todo)
}