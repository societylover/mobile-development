package ru.test.l7_remotedatabase.model

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideDatabaseRef() : DatabaseReference =
            FirebaseDatabase.getInstance("https://todo-1f7d5-default-rtdb.europe-west1.firebasedatabase.app")
                .getReference("todos")
}