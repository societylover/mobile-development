package ru.test.l1interface

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val progress = remember { progressNonMutable }
            val operationValue = remember { progressStep }

            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally) {
                LinearProgressIndicator(progress = progress.value,
                    modifier = Modifier
                        .wrapContentWidth()
                        .height(45.dp)
                        .padding(0.dp, 0.dp, 0.dp, 30.dp)
                        .clip(RoundedCornerShape(4.dp)))

                Row(horizontalArrangement = Arrangement.SpaceBetween, modifier = Modifier.padding(40.dp)){

                    ButtonIncDec(-1, context = applicationContext)

                    TextField(value = (operationValue.value).toString(),
                        onValueChange = { operationValue.value = it.toFloat() },
                        modifier = Modifier.weight(3f),
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))

                    ButtonIncDec(1, context = applicationContext)
                }
            }
        }
    }

    companion object {
        var progressNonMutable = mutableStateOf(0.0f)
        var progressStep = mutableStateOf(0.1f)
    }
}

@Composable
fun ButtonIncDec(numModifier: Int, context: Context) {
    val symbol = if (numModifier < 0) '-' else '+'
    Button(onClick = {
        MainActivity.progressNonMutable.value = (MainActivity.progressNonMutable.value + numModifier * (MainActivity.progressStep.value / 100)).mod(1.0f) ;
        Toast.makeText(context, "$symbol${MainActivity.progressStep.value}", Toast.LENGTH_LONG).show() },
        modifier = Modifier
            .clip(RoundedCornerShape(10.dp))
            .padding(5.dp).width(40.dp) ) {
        Text(text = symbol.toString(), textAlign = TextAlign.Center)
    }
}