package ru.test.l7_remotedatabase.model

import androidx.annotation.WorkerThread
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import javax.inject.Inject

class TodoRepository @Inject constructor(private val database: DatabaseReference) {

    var allTodos: MutableList<Todo> = mutableListOf()
    private var dataSubscriber: FirebaseSubscriber? = null

    init {
        database.addListenerForSingleValueEvent(create { this.addAllTodos(it) } )
        database.addValueEventListener(create { allTodos.clear(); this.addAllTodos(it); })
    }

    private fun locallyAddTodo(todo: Todo) = allTodos.add(todo)

    private fun addAllTodos(snapshot: DataSnapshot) {
        if (snapshot.exists())
            snapshot.children.forEach{
                val value = it.getValue(Todo::class.java) as Todo
                value.key = it.key!!
                locallyAddTodo(value) }

        dataSubscriber?.updateToRemoteDataChanged(allTodos)
    }

    fun subscribe(subscriber: FirebaseSubscriber) {
        dataSubscriber = subscriber
    }

    fun unsubscribe() {
        dataSubscriber = null
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(todo: Todo) {
        val id = database.push().key.toString()
        database.child(id).setValue(todo)
        allTodos.add(todo)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun delete(todo: Todo) {
        database.child(todo.key).removeValue()
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(todo: Todo) {
         database.child(todo.key).setValue(todo)
    }
}