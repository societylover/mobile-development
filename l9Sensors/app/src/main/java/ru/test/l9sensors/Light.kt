package ru.test.l9sensors

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener

class Light(context: Context, listener: LightListener) : SensorBasic(context, Sensor.TYPE_LIGHT) {

    init {
        sensorEventListener = object : SensorEventListener {
            override fun onSensorChanged(p0: SensorEvent) {
                listener.onLightChanged(p0.values[0])
            }

            override fun onAccuracyChanged(p0: Sensor?, p1: Int) { }
        }
    }

    companion object {
        interface LightListener {
            fun onLightChanged(lightLevel: Float)
        }
    }
}