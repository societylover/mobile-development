package ru.test.l6_localbase.view

import ru.test.l6_localbase.model.Todo

interface ItemButtonsCallback {
    fun updateTodo(todo: Todo)
    fun updateTodoDate(todo: Todo)
    fun removeTodo(todo: Todo)
}
