package ru.test.l6_localbase.model

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun getDatabase(@ApplicationContext app: Context) = Room.databaseBuilder(
        app,
        TodoDatabase::class.java,
        "TodoDatabase"
    ).fallbackToDestructiveMigration()
        .build()


    @Singleton
    @Provides
    fun provideDao(database: TodoDatabase) = database.todoDao()
}