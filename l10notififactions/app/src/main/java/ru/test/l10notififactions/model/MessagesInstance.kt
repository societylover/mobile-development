package ru.test.l10notififactions.model

import androidx.lifecycle.MutableLiveData

object MessagesInstance {
    val messages: MutableLiveData<Message> = MutableLiveData()
}