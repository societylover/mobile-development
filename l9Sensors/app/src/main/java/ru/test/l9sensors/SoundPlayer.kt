package ru.test.l9sensors

import android.content.Context
import android.media.MediaPlayer

class SoundPlayer(private val context: Context) {

    private var mediaPlayer = MediaPlayer.create(context, R.raw.back_music)
    private var lastSoundPlayed: Int = R.raw.back_music

    private fun stop() {
        mediaPlayer.stop()
    }

    private fun play(resid: Int) {
        if (lastSoundPlayed != resid) {
        lastSoundPlayed = resid
        stop()
        mediaPlayer.release()
        mediaPlayer = MediaPlayer.create(context, resid)
        mediaPlayer.start()
        }
    }

    fun playLight() {
        play(R.raw.light_music)
    }

    fun playDark() {
        play(R.raw.dark_music)
    }

    fun playUp() {
        play(R.raw.up_music)
    }
    fun playDown() {
        play(R.raw.down_music)
    }
    fun playForward() {
        play(R.raw.front_music)
    }
    fun playBack() {
        play(R.raw.back_music)
    }
}