package ru.test.l10notififactions.model

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import ru.test.l10notififactions.model.MessagesInstance.messages

class FirebaseMessaging : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        messages.postValue(Message(p0.from,
            p0.data.entries.joinToString(", ") { it.value }, p0.sentTime))
        super.onMessageReceived(p0)
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }
}