package ru.test.l7_remotedatabase.view

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import ru.test.l7_remotedatabase.model.DateParameters.getCurrentDateParameters
import ru.test.l7_remotedatabase.model.Todo

class DatePickerFragment(var todo: Todo,
                         val todoSaveCallback: (Todo)->Unit) : DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val date = getCurrentDateParameters()
        return DatePickerDialog(requireActivity(), this, date.third, date.second, date.first)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        todo.datetime = "$dayOfMonth\n${month+1}\n$year"
        todoSaveCallback(todo)
    }
}