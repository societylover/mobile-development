package ru.test.l5_camera

import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy

class LightAnalyzer(private val listener: (light: Double) -> Unit) : ImageAnalysis.Analyzer {

    override fun analyze(image: ImageProxy) {
        val buffer = image.planes[0].buffer
        val data = buffer.toByteArray()
        val pixels = data.map { it.toInt() and 0xFF }
        val light = pixels.average()

        listener(light)
        image.close()
    }
}