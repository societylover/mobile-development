package ru.test.l5camera

import android.annotation.SuppressLint
import android.util.Log
import androidx.appcompat.widget.AppCompatTextView
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.common.InputImage

// Класс-обработчик данных с камеры
class ImageProcessing(private val textView: AppCompatTextView) {
    // Формат сканируемых кодов
    val options = BarcodeScannerOptions.Builder().setBarcodeFormats(Barcode.FORMAT_QR_CODE).build()

    @SuppressLint("UnsafeOptInUsageError")
    fun processImageProxy(barcodeScanner: BarcodeScanner, imageProxy: ImageProxy) {
        val inputImage = InputImage.fromMediaImage(imageProxy.image!!, imageProxy.imageInfo.rotationDegrees)

        barcodeScanner.process(inputImage)
            .addOnSuccessListener { barcodes->
                barcodes.forEach {
                    val bounds = it.boundingBox
                    val corners = it.cornerPoints

                    println("${bounds.left} ${bounds.top} ${bounds.right} ${bounds.bottom}")
                    println("textViewBounds: ${textView.left} ${textView.top} ${textView.right} ${textView.bottom}")

                    textView.text = it.rawValue

                        // Надо бы было, если бы параметром setBarcodeFormats было Barcode.FORMAT_ALL_FORMATS
//                    val valueType = it.valueType
//                    when (valueType) {
//                        Barcode.TYPE_WIFI -> {
//                            val ssid = it.wifi!!.ssid
//                            val password = it.wifi!!.password
//                            val type = it.wifi!!.encryptionType
//                            textView.text =
//                                "ssid: $ssid\npassword: $password\ntype: $type"
//                        }
//                        Barcode.TYPE_URL -> {
//                            val title = it.url!!.title
//                            val url = it.url!!.url
//
//                            textView.text = "Title: " + title + "\nURL: " + url
//                        }
//                    }

                }
            }.addOnFailureListener{
                Log.e(TAG, it.message ?: it.toString())
            }.addOnSuccessListener {
                // Когда было проанализировано 1 изображение, закроем его
                //closed it by calling ImageProxy.close()
                imageProxy.close()
            }
    }

    companion object {
        const val TAG = "ImageProcessing"
    }
}