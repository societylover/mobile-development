package ru.test.l7_remotedatabase.model

import ru.test.l7_remotedatabase.model.DateParameters.getCurrentDateParametersAsString

data class Todo (
    var description: String = "",
    var datetime: String? = getCurrentDateParametersAsString(),
    var key: String = "")
